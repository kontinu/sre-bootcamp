terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
}


# # * provider block
# provider "docker" {
#   alias = "docker"
# }
