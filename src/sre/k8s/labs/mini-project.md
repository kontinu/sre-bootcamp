# Mini Project 🧑‍🔬

## Description.
I have a Python+Flask application that basically does the following:


```mermaid
graph TD;
   User-- "browser at http://localhost:5000" -->pythonApp-- "connects to redis (counts visits)" --->Redis
```


Very simple, application that counts its visits using Redis database. If there's no Redis connection setup the app will still work but it'll show  =='No Redis Connection'==


Here's some important things to notice:

- the app listens on port 5000.
- REDIS_HOST is the environment variable to connect to a redis deployed, ==NO== need to add the Redis port 6379
- There's another environment variable that is displayed in the Web UI `FOO`, you can add whatever value you like to see it displayed in the webpage.

!!! Note
    Here's an example of how the REDIS_HOST variable looks like, supposed there's a redis with the following FQDN redis.example.com
    `REDIS_HOST=redis.example.com`


## Instructions

Create a namespace called `mini-project`


### Redis

- setup a simple Redis ==Deployment== with a ==Service==.
- 1 replica
- nothing fancy

!!! Help
    Here's a little Help
```bash
# deploy redis
kubectl apply -f https://k8s.io/examples/application/guestbook/redis-leader-deployment.yaml -n mini-project
# deploy the Redis service
kubectl apply -f https://k8s.io/examples/application/guestbook/redis-leader-service.yaml -n mini-project
```




### Application

- [ ] Create a Deployment for the application.
- [ ] 1 replica
- [ ] Image is: `kontinu/docker-redis:v1.0`
- [ ] Port 5000
- [ ] Service type NodePort
- [ ] Service port 8080
- [ ] Inspect the redis service to get the name of the service.
- [ ] Create a Configmap with values for `REDIS_HOST` (the name from previous step) and `FOO`
- [ ] Inject the configmap as environment variables using `envFrom`; here's an [example (follow only the configmap example not the secret one)](https://gist.github.com/troyharvey/4506472732157221e04c6b15e3b3f094)



!!! Reminder

    you can always test your app locally by using Docker.
    ```bash
    docker run -it -p 5000:5000 -e "ENV_VAR=env_var_value"  kontinu/docker-redis:v1.0
    ```
