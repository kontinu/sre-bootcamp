#!/bin/bash
set -e


: ${wait_time:="1"}
: ${text:=""}
: ${conn:="http://ifconfig.me"}

arg=$@

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
    echo "** Trapped CTRL-C"
}

for i in {1..10};do
    echo -e "\n[$i] Hello from bash $arg"
    sleep ${wait_time}
done

