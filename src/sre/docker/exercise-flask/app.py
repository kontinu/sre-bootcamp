from flask import Flask
from redis import Redis
import os

REDIS_HOST = os.getenv('REDIS_HOST', "redis")
app = Flask(__name__)
redis = Redis(host=REDIS_HOST, port=6379)


@app.route('/') #http://localhost/
def hello():
    redis.incr('hits')
    counter = str(redis.get('hits'),'utf-8')
    message = os.getenv('MESSAGE', "default")
    return "This webpage has been viewed "+counter+" time(s) -- {}".format(message)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
