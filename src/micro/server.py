#!/usr/bin/env python3
import os, redis, time ,platform, threading, random, sys
from flask import Flask, make_response, render_template, jsonify
from functools import wraps, update_wrapper
from datetime import datetime,timedelta



if os.getenv('OTEL_EXPORTER_OTLP_HEADERS', False):
  print("Enabling OTEL")
  from opentelemetry import trace
  from opentelemetry.instrumentation.flask import FlaskInstrumentor
  from opentelemetry.instrumentation.requests import RequestsInstrumentor
  from opentelemetry.exporter.otlp.proto.http.trace_exporter import OTLPSpanExporter
  from opentelemetry.sdk.trace import TracerProvider
  from opentelemetry.sdk.trace.export import BatchSpanProcessor
  # Initialize tracing and an exporter that can send data to Honeycomb
  provider = TracerProvider()
  processor = BatchSpanProcessor(OTLPSpanExporter())
  provider.add_span_processor(processor)
  trace.set_tracer_provider(provider)
  tracer = trace.get_tracer(__name__)


app = Flask(__name__)

if os.getenv('OTEL_EXPORTER_OTLP_HEADERS', False):
  FlaskInstrumentor().instrument_app(app)
  RequestsInstrumentor().instrument()

app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0



REDIS_HOST = os.getenv('REDIS_HOST', 'redis')
hostname = platform.node()


redisClient = redis.Redis(host=REDIS_HOST, port=6379, charset="utf-8", decode_responses=True)


class Worker:
  prefix = "micro:"
  ttl=10

  def __init__(self, REDIS_HOST, hostname, stop_threads=False):
    self.REDIS_HOST=REDIS_HOST
    self.counter=Worker.set_id()
    self.key=f"{Worker.prefix}{hostname}"

    self.stop_threads=stop_threads
    self.visits=0
    print(f"Starting ${self.key} = {self.counter}")
    global redisClient

    value={"counter": self.counter, "visits": self.visits}
    redisClient.hset(self.key, mapping=value)
    redisClient.expire(name=self.key, time=Worker.ttl)

    self.my_thread=None
    thread_id=self.start()


  def task(self):
    print(f"Running task with {self.counter}")
    retries = 5
    global redisClient
    while True:
      if self.stop_threads:
        print("Stopping task")
        break
      time.sleep(random.uniform(0.5, 2.0))
      try:
        value={"counter": self.counter, "visits": self.visits}
        redisClient.hset(self.key, mapping=value)
        redisClient.expire(name=self.key, time=Worker.ttl)
        self.counter+=1
      except redis.exceptions.ConnectionError as exc:
        if retries == 0:
          print("'No Redis Connection'")
        retries -= 1
    print("Exiting task...")



  def start(self):
    print(f"Starting ${self.key} = {self.counter}")
    self.stop_threads=False
    self.my_thread=threading.Thread(target=self.task, daemon=True)
    self.my_thread.start()
    return self.my_thread.ident

  def stop(self):
    self.stop_threads=True
    time.sleep(1)

  def get_status(self):
    global redisClient
    try:
      value={}
      value = { k: int(v) for k,v in redisClient.hgetall(self.key).items() }
      print(value)

      return { self.key : value }
    except:
      return f"{self.key} Not Found"

  @staticmethod
  def set_id():
    if os.getenv('ID'):
      return int(os.getenv('ID'))
    else:
      return sum([int(s) for s in hostname.split() if s.isdigit()])

  def set_visits(self):
    self.visits+=1
    value={"counter": self.counter, "visits": self.visits}
    redisClient.hset(self.key, mapping=value)
    redisClient.expire(name=self.key, time=Worker.ttl)



worker=Worker(REDIS_HOST, hostname)


@app.route('/')
def hello():
  # return a json file?
  global worker
  worker.set_visits()
  return jsonify(worker.get_status())



@app.route('/health')
def health():
  return "Im healthy"


@app.route('/stop')
def stop():
  global worker
  worker.stop()
  return "Stopping worker."



@app.route('/start')
def start():
  global worker
  worker.start()
  return "Starting worker."


if __name__ == "__main__":
  app.run(host="0.0.0.0")
