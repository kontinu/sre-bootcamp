

# Final Project

## Groups

![Groups](../static/img/grupos.png)

### Requirements

- Have access to AWS and/or GCP account.
- Have gcloud and/or aws cli installed.
- Obviously have all the requirements installed (terraform, docker, etc)


!!! warning
    PLEASE READ ALL INSTRUCTIONS.

### Common instructions

!!! note inline end
    You can even create and organization in your gitlab account with the same "group" name

- [X] Create groups of 3 or 2
- [X] Name your group (with a cool name ;) )

- [X] We will deploy to Cloud (AWS or GCP any of your preference)
- [X] We will have 2 ["tracks"](#tracks) 🛤️ (Kubernetes and Legacy VMs approach)
- [X] Let's go to the wheel of names https://wheelofnames.com/swj-ugn
- [X] Everything should happen or be developed in your own fork of the repo.
- [X]  🌞 Make sure to modify the application source code for the frontend `index.html`
      1. change the `Title <h1> </h1>` of the main page to include your names
      2. change `footer` to include your names
      3. change colors in the `css style` section
- [X]  [Pull latest changes](#update-fork) from the original repo
- [X]  Add CI/CD pipeline (.gitlab-ci.yml)
      1. Build a docker image and publish to gitlab-ci container registry ( Job is already provided you just need to uncomment it ) [Mandatory]
      2. Feel free to add any other type of job that you want to showcase your skills. (SAST, security scans, etc) [ Optional ]
      3. this will be the last mile of the CI pipeline, `src code => docker image => container registry`

- [X] All new files created by you should have comments where you document what they do and why they are needed.
- [X] Add your own README.md that explains your approach.

!!! info "CD pipeline"
    it will be different approach for each of the 2 tracks





## Tracks 🛤️

Once you have your CI pipeline and the output is a valid docker image, you can start with the CD pipeline, infrastructure as code, and deployment.

### 1. "Legacy Cloud ⛅"
This project will run in a legacy cloud environment, you will need to create a VM or set of VMs and deploy the application there, you will probably need to add a L3 or L7 loadBalancer, and a Redis database (can be in a VM or a managed service)

- [x] Create a separate repository for the infrastructure code. (you can create an organization in gitlab and add the repo there)
- [x] Use Terraform to create the suggested infrastructure
- [x] Use Atlantis locally or one that is deployed to your cloud environment to manage the infrastructure as code via pull-requests (be careful 🔥 that atlantis will be publicly accessible, so locally is preferred )
- [X] Once you have the infrastructure deploy your application using docker-compose, just take into account that redis database should be shared between the multiple containers that you will deploy. (so a single Redis instance for all services)
- [X] [Optional] use CloudMemoryStore or ElastiCache can be used, but provisioned using Terraform as well
- [X] **Automate** the process of deployment, either create a pipeline in gitlab to deploy to the environment or create a script that can be run locally to deploy the application to the environment once it detects the new "docker-image" or both, or even use terraform provisioners
- [X] To monitor we will use CloudWatch or StackDriver, so make sure to enable the API to monitor VMs.


!!! tip "Extra 🌟"

    - instead of single VMs, you can use ASGs (auto-scaling groups) or MIG (managed instance groups)
    - you can use Packer to generate VM AMIs or Images
    - Use Ansible to deploy a new version of the image


![image](../static/img/architecture.png)

### 2. "Kubernetes Cloud 🕸️"

!!! info inline end
    GKE is the recommended Kubernetes environment for this project.

This project will run in a "modern" cloud environment, you will need to create a Kubernetes as a service cluster (GKE or EKS or even locally via minikube)

- [X] We will deploy the app to kubernetes using GitOps (Helm Charts **recommended**, kustomize or pure manifests)
- [X] Create a separate repository for the configuration code (Helm Charts, kustomize or pure manifests). (you can create an organization in gitlab and add the repo there)
- [X] Deploy and configure ArgoCD for the cluster
- [X] Configure ArgoCD to read changes from the repository that you created in the previous step (create ArgoCD Application, can be done manually or using [ArgoCD CRDs](https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/#applications))
- [X] Demo a full CI/CD pipeline
- [X] For the manifests please use Helm Charts
- [X] For Redis can be using dependencies in the Chart.yaml file, Obviously using the correct configuration in values.yaml

  ```yaml
  dependencies:
  - name: redis
    version: 17.0.X
    repository: https://charts.bitnami.com/bitnami
  ```

- [x] 🌶️ Add extra sauce to your helm repo, expose other things

- [X] Add an Ingress controller to the cluster (Nginx or Traefik, this can also be automated using gitops)
- [X] To monitor add a grafana/prometheus release using helm (this can be also automated using gitops and also as dependency)
- [x] Add IaC terraform to create your k8s cluster creation. (a new repository) [own repository]
- [x] Add atlantis to your terraform repository. (local atlatis)

!!! tip "Extra 🌟"

    - Add service-mesh to your cluster (linkerd)
    - Add Continous deployment when a new image is generated, you can modify the deployment repository with the new image in the values.yaml



---
## How to deliver?

!!! info "source code"
    Feel free to modify the source code at your own discretion, you can add new features, change the UI, add new services, etc. (but make sure to document it in your README.md)


- **Repo(s):** Obviously your source code repositories
- **Presentation:** You will have 20~30 minutes to deliver your project with a final presentation, think of it as a pitch, use slides, use non technical language so that business people could understand it, and present your repository, explain what problems you faced and why you chose to solve the problems the way you chose.
- **Classroom:** also upload screenshot**s** of your project to classroom assignment.
