# Kubernetes Intro

we can translate the current docker-compose to kubernetes.

- [compose service] Python App => Deployment + ConfigMap + Service
- [compose service] Python Micro => Deployment + ConfigMap + Service
- [compose service] Redis => StatefulSet + ConfigMap + HeadlessService
- [compose network] myapp => Namespace


## Excercise

```
git clone https://gitlab.com/kontinu/sre-bootcamp.git
cd sre-bootcamp

# create a directory for the manifests
mkdir -p k8s/manifests
```

## Redis in Kubernetes
the work has already been done, so go and take a look

```

cat src/sre/k8s/manifests/redis.yaml

```

## ExanmpleApp in K8s

we will start translating the docker-compose to kubernetes.

=== "Docker Compose"

    ``` yaml
    # DNS for service discovery, translated to Service.metadata.name
    example-app:
      # translated to spec.template.spec.containers
      image: ${APP_DOCKER_IMAGE:-registry.gitlab.com/kontinu/sre-bootcamp/master/app}
      # no direct translation as Deployment is the actual controller.
      restart: always
      # no direct translation, kubernetes "can't" handle this.
      build:
        context: app
        dockerfile: Dockerfile
      # Env vars translated to spec.template.spec.containers.env
      environment:
        - REDIS_HOST=redis

      # no translation needed, in K8s are part of the same subnet, even when they live in differente namespaces.
      networks:
        - myapp
      # translated to spec.template.spec.containers.ports and we use Service for it to be accessible.
      ports:
        - "8080:5000"


    ```

=== "Deployment"

    ``` yaml
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: example-app
      # non functional labels
      # don't need to be the same.
      labels:
        app: example-app
    spec:
      replicas: 1
      selector:
        matchLabels:
          # should match template.metadata.labels
          app: example-app
      template:
        metadata:
          # should match spec.selector.matchLabels
          labels:
            app: example-app
        spec:
          containers:
            - name: mycontainer
              # Env vars
              env:
                - name: REDIS_HOST
                  value: redis
                #! suppose we need a password, for passwords we better use secrets.
                - name: DB_PASSWORD
                  value: th3Passw0rd
              #! neve use latest!
              image: registry.gitlab.com/kontinu/sre-bootcamp/master/app
              ports:
              #* Our app listens on port 5000
                - containerPort: 5000
              resources: {}

    ```

=== "Service"

    ``` yaml
    apiVersion: v1
    kind: Service
    metadata:
      name: example-app
      labels:
        # non functional
        service: for-example-app
    spec:
      # internal load balancer if we use ClusterIP, NodePort exposes the service to the nodes/vms.
      type: NodePort # ClusterIP | NodePort | LoadBalancer
      ports:
        - port: 80 # port of the service itself
          targetPort: 5000 # same as containerPort
      selector:
        # should match Pod labels.
        app: example-app

    ```


---
## Further Reading

- [kustomize](https://kustomize.io/)
- [Helm](https://helm.sh/)
- [Kubernetes documentation](https://kubernetes.io/)
- [Ingress](https://kubernetes.github.io/ingress-nginx/)
