
Feel free to navitate to `src/` and check the contents of our app.



Our app is a very simple application that is composed of:

- [**Backend**] Python Flask worker application that auto sets a starting number, summing up all numbers in the hostname (ie: e1a7734fa926 => 39), and starting a auto counter every random seconds, it also records the amount of unique visits each container has (even though they are loadbalanced)

!!! info
    When you click on the button or visit the Ingress URL for the micro app it will return a json file with the described information.

- [**Frontend**] Python Flask application that displays data retrieved from redis for each container (Pod) worker; visits per container (even if they are load balanced) and auto counter.

- [**Database**] Redis container that stores the data from the worker application

- [**Reverse proxy using traefik**] Traefik that will route traffic to the frontend and backend

Our app is fully containerized and its configuration is declared in a `docker-compose.yml` file, its configuration is as following




### Run the app individually using docker

```bash
docker image build -f src/app/Dockerfile -t frontend src/app
docker image build -f src/micro/Dockerfile -t micro src/micro

# create a network to allow service discovery
docker network create --driver bridge myapp

docker container run --network=myapp --rm -d  --name redis redis:alpine
docker container run --network=myapp -e "REDIS_HOST=redis" -e "MICRO_URL=http://localhost:5050" --rm -d  --name frontend -p 5000:5000 frontend
docker container run --network=myapp  -e "REDIS_HOST=redis" --rm -d  --name micro -p 5050:5000 micro

```


### Or using docker-compose 🚀

```bash
docker-compose -f src/docker-compose.yml up --build
```

!!! tip
    hope you see why docker-compose is so important and simplifies things!!!

    you declare the interaction between your "services/containers", network, configs, variables, port bindings, etc.




```yaml
version: "3.5"

services:
  traefik:
    image: "traefik:v2.8"
    container_name: "traefik"
    command:
      #- "--log.level=DEBUG"
      - "--api.insecure=true"
      - "--providers.docker=true"
      - "--providers.docker.exposedbydefault=false"
      - "--entrypoints.web.address=:80"
    ports:
      - "80:80"
      - "8080:8080"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock:ro"

  web:

    image: ${IMAGE_NAME:-registry.gitlab.com/kontinu/sre-bootcamp/new-app-model/app}
    build:
      context: app/
    environment:
      - REDIS_HOST=redis
      - FOO=${FOO:-BAR}
      - MICRO_URL=http://${SUBDOMAIN:-app}.traefik.me/micro

    # adds self-healing availability
    healthcheck:
      test: wget -O- http://localhost:5000/health
      interval: 15s
      timeout: 3s
      retries: 3
      start_period: 2s
    labels:
      - "traefik.enable=true"
      - "traefik.http.services.app.loadbalancer.server.port=5000"
      - "traefik.http.routers.app.rule=Host(`${SUBDOMAIN:-app}.traefik.me`)"
      - "traefik.http.routers.app.entrypoints=web"

  micro:
    image: ${IMAGE_NAME:-registry.gitlab.com/kontinu/sre-bootcamp/new-app-model/micro}
    build:
      context: micro
    environment:
      - REDIS_HOST=redis
    healthcheck:
      test: wget -O- http://localhost:5000/health
      interval: 15s
      timeout: 3s
      retries: 3
      start_period: 2s
    labels:
      - "traefik.enable=true"
      - "traefik.http.services.micro.loadbalancer.server.port=5000"
      - "traefik.http.routers.micro.rule=Host(`${SUBDOMAIN:-app}.traefik.me`) && PathPrefix(`/micro`)"

      - "traefik.http.routers.micro.middlewares=micro-stripprefix"
      - "traefik.http.middlewares.micro-stripprefix.stripprefix.prefixes=/micro"
      - "traefik.http.routers.micro.entrypoints=web"


  redis:
    image: "redis:alpine"
    deploy:
      replicas: 1
    ports:
      - "6379:6379"
```



# Build the app

```
docker-compose -f src/docker-compose.yml build
```

# Run the app

```
docker-compose -f src/docker-compose.yml up
# add -d to run in detach mode
```

# Running scaled micro service in docker-compose

```
cd src/
docker-compose up --build --scale micro=5
```

---


