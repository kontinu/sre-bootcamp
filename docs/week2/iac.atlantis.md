# Infrastructure as Code with vitamins 💊


Shifting left is something we strive to achieve, whats more dev than pull-requests?

as SREs we want to also keep learning, in this lab we will learn about [Atlantis](https://www.runatlantis.io/)


!!! tip
    I assume you already have everthing in the [Requirements](../requirements.md) installed 

> Note: You can also follow the instructions [here](https://www.runatlantis.io/guide/testing-locally.html)


# Download Atlantis

Get the latest release from https://github.com/runatlantis/atlantis/releases and unpackage it.


# Download Ngrok


Atlantis needs to be accessible somewhere that github.com/gitlab.com/bitbucket.org or your GitHub/GitLab Enterprise installation can reach. One way to accomplish this is with ngrok, a tool that forwards your local port to a random public hostname.

Go to https://ngrok.com/download, download ngrok and unzip it.

Start ngrok on port 4141 and take note of the hostname it gives you:

```bash
./ngrok http 4141
```

In a new tab (where you'll soon start Atlantis) create an environment variable with ngrok's hostname:


```bash
URL="https://{YOUR_HOSTNAME}.ngrok.io"
```

# Create a Webhook Secret
GitHub and GitLab use webhook secrets so clients can verify that the webhooks came from them.

WARNING

Bitbucket Cloud (bitbucket.org) doesn't use webhook secrets so if you're using Bitbucket Cloud you can skip this step. When you're ready to do a production deploy of Atlantis you should allowlist Bitbucket IPs to ensure the webhooks are coming from them.

Create a random string of any length (you can use https://www.random.org/strings/) and set an environment variable:

```bash
SECRET="{YOUR_RANDOM_STRING}"
```


# Add webhook

## GitLab or GitLab Enterprise Webhook

- Go to your repo's home page
- Click Settings > Webhooks in the sidebar
- set URL to your ngrok url with `/events` at the end. Ex. https://c5004d84.ngrok.io/events
- double-check you added `/events` to the end of your URL.
- set Secret Token to your random string
- check the boxes:
    - [x] Push events
    - [x] Comments
    - [x] Merge Request events
- leave Enable SSL verification checked
- click Add webhook


# Create an access token for Atlantis


## GitLab or GitLab Enterprise Access Token

follow https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html#create-a-personal-access-token

- create a token with api scope
- set the token as an environment variable

```
TOKEN="{YOUR_TOKEN}"
```

# Start atlantis

```bash
USERNAME="{the username of your GitHub, GitLab or Bitbucket user}"
REPO_ALLOWLIST="$YOUR_GIT_HOST/$YOUR_USERNAME/$YOUR_REPO"
# ex. REPO_ALLOWLIST="github.com/runatlantis/atlantis"
# If you're using Bitbucket Server, $YOUR_GIT_HOST will be the domain name of your
# server without scheme or port and $YOUR_USERNAME will be the name of the **project** the repo
# is under, **not the key** of the project.
```

## Gitlab Command

```bash
atlantis server \
--atlantis-url="$URL" \
--gitlab-user="$USERNAME" \
--gitlab-token="$TOKEN" \
--gitlab-webhook-secret="$SECRET" \
--repo-allowlist="$REPO_ALLOWLIST"
```

# Our repo

Our repo is already prepared to work with atlantis via the `atlantis.yaml`

```yaml
version: 3
projects:
- dir: src/sre/iac/terraform-atlantis
  delete_source_branch_on_merge: true
  autoplan:
    enabled: true
  apply_requirements: [approved]
```


# Create a pull request
Create a pull request so you can test Atlantis.

!!! tip

    Change the file `src/sre/iac/terraform-atlantis/main.tf` to test Atlantis.

    Add a whitespace, change the name of the resource, etc.

# Autoplan

You should see Atlantis logging about receiving the webhook and you should see the output of terraform plan on your repo.
