# IaC Docker

Build, change, and destroy Docker infrastructure using Terraform.

## Hands-on

Read the files before running all the commands



```
cd src/sre/iac/terraform-docker
terraform init

# Plan
terraform plan

# Apply the infrastructure

terraform apply

# Show the created infrastructure

terraform show
```

### Modify infrastructure


go and change main.tf

```diff
labels {
    label = "environment"
-   value = "development"
+   value = "local"
  }

```

```
# Plan
terraform plan

# Apply the infrastructure

terraform apply
```


### Cleanup

```
terraform destroy
```

