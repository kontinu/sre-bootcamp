# Ingress

Install [Nginx Ingress controller](https://kubernetes.github.io/ingress-nginx/)

=== "GCP"

    ```
    kubectl create clusterrolebinding cluster-admin-binding \
    --clusterrole cluster-admin \
    --user $(gcloud config get-value account)

    kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.0/deploy/static/provider/cloud/deploy.yaml
    ```

=== "AWS"

    ```
    kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.0/deploy/static/provider/aws/deploy.yaml

    # Download the the deploy-tls-termination.yaml template:
    wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.0/deploy/static/provider/aws/deploy-tls-termination.yaml

    # Edit the file and change the VPC CIDR in use for the Kubernetes cluster:
    proxy-real-ip-cidr: XXX.XXX.XXX/XX

    # Change the AWS Certificate Manager (ACM) ID as well:
    arn:aws:acm:us-west-2:XXXXXXXX:certificate/XXXXXX-XXXXXXX-XXXXXXX-XXXXXXXX

    # Deploy the manifest:
    kubectl apply -f deploy-tls-termination.yaml
    ```

=== "Minikube"

    ```
    # minikube start --driver=hyperkit
    minikube addons enable ingress
    ```

=== "DockerDesktop and all Kubernetes"

    ```
    helm upgrade --install ingress-nginx ingress-nginx \
    --repo https://kubernetes.github.io/ingress-nginx \
    --namespace ingress-nginx --create-namespace
    ```
